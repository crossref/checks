# Checks

The checks here run on a schedule to ensure that no regressions are introduced into the Crossref system. Check the CI builds for success.

Initially for Distributed Usage Logging.

## Configuration

The following environment variables should be set. Either add these when you run locally or ensure they are in the GitLab CI environment.

 - `PLUS_API_TOKEN`

## To run

Designed to run automatically in Gitlab CI. To run locally:

Install dependencies:

	pip3 install -r requirements.txt

Then to run a specific test:

    PLUS_API_TOKEN=«INSERT HERE» python3 -m unittest tests/distributed_usage_logging.py

