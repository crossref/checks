from unittest import TestCase
import requests
import os
from datetime import date, timedelta

FILENAMES = [
    "all.xml.tar.gz",
    "all.json.tar.gz",
]

BASE = "https://api.crossref.org/snapshots/"

HEADERS = {"Crossref-Plus-API-Token": "Bearer " + os.environ["PLUS_API_TOKEN"]}

# GiB in bytes
GIB = (1024 * 1024 * 1024)

# Heuristic for minumum size
MIN_SIZE_GIB = 10


def retrieve_monthly(month, filename):
    "Retrieve a monthly snapshot, return as a Requests response with streamed file."
    url = BASE + "monthly/" + month + "/" + filename
    print("Retrieve", url)
    response = requests.get(url, headers=HEADERS, stream=True)
    return response


def previous_month(today):
    "Given today's date, return the first day of last month."

    # First day of this month.
    start_of_month = date(year=today.year, month=today.month, day=1)

    # The day before is the last day of previous month.
    last_month = start_of_month - timedelta(days=1)

    first_of_last_month = date(year=last_month.year,
                               month=last_month.month,
                               day=1)
    return first_of_last_month


def date_format(date):
    return date.strftime("%Y/%m")


class UtilsTests(TestCase):
    def test_previous_month(self):
        self.assertEqual(
            previous_month(date(2019, 10, 1)),
            previous_month(date(2019, 10, 20)),
            "Previous month returns doesn't care what day of the month it is, returns the same."
        )

        self.assertEqual(
            previous_month(date(2019, 10, 10)), date(2019, 9, 1),
            "Previous month returns first of September for 10th of October")

        self.assertEqual(
            previous_month(date(2019, 1, 11)), date(2018, 12, 1),
            "Previous month returns first of December 2018 for 1st of January")

    def test_date_format(self):
        self.assertEqual(date_format(date(2019, 1, 1)), "2019/01",
                         "Date formatint conforms to expected API format.")


class MonthlySnapshots(TestCase):
    def test_latest_month_present(self):
        "The 'latest' labelled month should always be present and non-empty."
        for filename in FILENAMES:
            response = retrieve_monthly("latest", filename)
            content_length = int(response.headers.get("Content-Length"))
            self.assertGreater(
                content_length, MIN_SIZE_GIB * GIB,
                "Content length of %s should be at least min size" % filename)
            response.close()

    def test_previous_month_present(self):
        "The previous month from now should always be present and non-empty."
        today = date.today()
        for filename in FILENAMES:
            month = date_format(previous_month(today))
            response = retrieve_monthly(month, filename)
            content_length = int(response.headers.get("Content-Length"))
            self.assertGreater(
                content_length, MIN_SIZE_GIB * GIB,
                "Content length of %s should be at least min size" % filename)
            response.close()

    def test_previous_month_equal_size_current(self):
        "The previous month from now should always have the same size as the 'latest' month because they should be the same thing."
        pass

        # TODO!

    def test_previous_month_greater_than_before(self):
        "The previous month from now should have a larger file size than the month that went before it."
        pass

        # TODO!
