from unittest import TestCase
import requests


def doi_head(doi):
    "Retrieve a HEAD request for the given DOI."
    return requests.head("https://doi.org/" + doi)


def doi_link_headers(doi):
    "Return the dict of link headers for a DOI."
    head = doi_head(doi)
    return head.headers


def dul_headers(headers):
    link_headers = headers['link']
    links = requests.utils.parse_header_links(link_headers)
    return filter(lambda x: x['rel'] == 'dul', links)


# A set of DOIs that should have DUL link headers.
# In future this can be queried live to make the test more dynamic.
DUL_TEST_DOIS = ["10.1016/j.heliyon.2018.e00965"]


class DistributedUsageLoggingCase(TestCase):
    def test_header_present(self):
        "DUL headers should be present with correct values."
        for doi in DUL_TEST_DOIS:
            headers = dul_headers(doi_link_headers(doi))
            for header in headers:
                self.assertEqual(
                    header['rel'], 'dul',
                    "All DUL headers should have the 'dul' relationship.")
                self.assertTrue(
                    header['url'],
                    "All DUL headers should have a non-blank URL string.")

    def test_headers_unique(self):
        "DUL headers should not contain duplicate entries."
        for doi in DUL_TEST_DOIS:
            headers = dul_headers(doi_link_headers(doi))
            urls = [x['url'] for x in headers]
            self.assertEqual(list(set(urls)), urls,
                             "There should be no duplicate DUL headers")
